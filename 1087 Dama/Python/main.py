def break_check(l):
    return not any(map(lambda x : x is not 0, l))

def zero_move_check(l):  # check if start and final positions are the same

    x1 = l[0]
    y1 = l[1]

    x2 = l[2]
    y2 = l[3]

    if (x1 == x2) and (y1 == y2):
        return True

def one_move_check(l):

    x1 = l[0]
    y1 = l[1]

    x2 = l[2]
    y2 = l[3]

    positions = []  # positions where one move is required

    for i in range(1, 9):  # add column and row of origin to possible positions
        positions.append([x1, i])
        positions.append([i, y1])

    for i in range(-8, 8):  # add column and row of origin to possible positions
        positions.append([x1+i, y1+i])
        positions.append([x1-i, y1+i])

    return [x2, y2] in positions

while True:
    input_list = list(map(int, input().split()))

    if break_check(input_list):
        break

    elif zero_move_check(input_list):
        print("0")

    elif one_move_check(input_list):
        print("1")

    else:
        print("2")
