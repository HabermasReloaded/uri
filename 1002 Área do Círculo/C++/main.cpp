#include <iostream>

#define PI 3.14159

double circle_area(double radius)
{
    double area;
    area = PI * radius * radius;

    return area;
}


int main()
{
    double radius;

    std::cin >> radius;

    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    std::cout.precision(4);
    std::cout << "A=" << circle_area(radius) << '\n';

    return 0;
}
