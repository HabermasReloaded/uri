#include <iostream>
#include <vector>


size_t vec_max_val(std::vector<size_t> num_vec)
{
    size_t max_val = num_vec[0];
    
    if(num_vec.size() == 1)
    {
        return max_val;
    }
    
    for(size_t i = 1; i < num_vec.size(); ++i)
    {
        if(num_vec[i] > max_val)
        {
            max_val = num_vec[i];
        }
    }
    
    return max_val;
}
    


int main()
{
    int inp_qty = -1;
    
    int linebreak_ctrl = 0;

    while(true)
    {
        std::cin >> inp_qty;
        
        if (inp_qty == 0)
        {
            return 0;
        }
        
        if (linebreak_ctrl == 1)
        {
            std::cout << '\n';
        }
        
        linebreak_ctrl = 1;
        
        std::string word;
        size_t word_len;

        std::vector<std::string> word_vec;
        std::vector<size_t> word_len_vec;
        std::vector<size_t> max_len_diff_vec;

        for(int i = 0; i < inp_qty; ++i)
        {
            std::cin >> word;
            word_len = word.length();

            word_vec.push_back(word);
            word_len_vec.push_back(word_len);
        }
        
        size_t max_word_len = vec_max_val(word_len_vec);
        
        for(auto i: word_vec)
            std::cout << std::string(max_word_len - i.length(), ' ') << i << '\n';
            
    }

    return 0;
}
