class Time:
    def __init__(self, hours, minutes):
        self.hours = int(hours)
        self.minutes = int(minutes) # convert inputs to ints if tey are strings

    def add_day(self):
        self.hours += 24

    def difference(self, another_time):

        if another_time.minutes > self.minutes: # if the minute deglta will be negative, add an hour to the minutes:
            self.minutes += 60
            self.hours -= 1

        self.hours -= another_time.hours
        self.hours %= 24 # so we dont get times > 24 or < 0

        self.minutes -= another_time.minutes

        if (self.hours == 0) and (self.minutes == 0):
            self.add_day() # game time cant be null, so we add a day if it is

    def print_time(self):
        print("O JOGO DUROU", self.hours, "HORA(S) E", self.minutes, "MINUTO(S)") # formatted output


begin_hour, begin_min, end_hour, end_min = input().split()

begin_time = Time(begin_hour, begin_min)
end_time = Time(end_hour, end_min)
game_time = end_time

game_time.difference(begin_time)
game_time.print_time()
