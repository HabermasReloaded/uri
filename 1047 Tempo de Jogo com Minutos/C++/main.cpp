#include <iostream>


int main()
{
    int begin_hour, begin_min, end_hour, end_min, game_time_hour, game_time_min;
    std::cin >> begin_hour >> begin_min >> end_hour >> end_min; // input values

    if ((begin_hour >= end_hour) and (begin_min >= end_min)) // check if the game ended earlier than it started
    {
        end_hour += 24; // add a day
    }

    if (begin_min > end_min)
    {
        end_min += 60;
        --end_hour;
    }

    // calculate time delta:

    game_time_hour = end_hour - begin_hour;
    game_time_min = end_min - begin_min;

    std::cout << "O JOGO DUROU " << game_time_hour << " HORA(S) E " << game_time_min << " MINUTO(S)" << std::endl; // print result

    return 0;
}
