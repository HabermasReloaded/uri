begin_hour, begin_min, end_hour, end_min = input().split() # split values in string and assign them to variables

begin_hour = int(begin_hour)
begin_min = int(begin_min)
end_hour = int(end_hour)
end_min = int(end_min)

if (begin_hour >= end_hour) and (begin_min >= end_min): # check if the game ended earlier than it started
    end_hour += 24 # add a day

 # if the minute delta will be negative, add an hour to the minutes:
if begin_min > end_min:
    end_min += 60
    end_hour -= 1

 # calculate time delta:
game_time_hour = end_hour - begin_hour
game_time_min = end_min - begin_min

print("O JOGO DUROU", game_time_hour, "HORA(S) E", game_time_min, "MINUTO(S)") # print result
