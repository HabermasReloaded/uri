begin_time, end_time = input().split() # split values in string and assign them to variables

 # convert input strings into ints:
begin_time = int(begin_time)
end_time = int(end_time)

if begin_time >= end_time: # if the time difference is negative or null
    end_time += 24 # add a day to end time

game_time = end_time - begin_time # game t is the delta of times
print("O JOGO DUROU", game_time, "HORA(S)") # print result
