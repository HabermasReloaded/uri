#include <iostream>


int main()
{
    int begin_time, end_time, game_time;

    std::cin >> begin_time >> end_time; // input values

    if(begin_time >= end_time) // if the time difference is negative or null
    {
        end_time += 24; // add a day to end time
    }

    game_time = end_time - begin_time; // game runtime is the delta of times

    std::cout << "O JOGO DUROU " << game_time << " HORA(S)" << std::endl; // print game time to std output

    return 0;
}
