before_sal = float(input())

if before_sal <= 400:
        raise_percent = 15

elif before_sal <= 800:
        raise_percent = 12

elif before_sal <= 1200:
        raise_percent = 10

elif before_sal <= 2000:
        raise_percent = 7

else:
        raise_percent = 4

after_sal = (1 + (raise_percent/100)) * before_sal # salary raise ammount + salary before raise:
after_sal = round(after_sal, 2) # rounding to 2 decimal places due to floating point errors
after_sal_out = "{0:.2f}".format(after_sal) # formatting answer string to always show 2 decimal places

 # salary raise ammount:
raise_amount = (raise_percent/100) * before_sal
raise_amount = round(raise_amount, 2)
raise_amount_out = "{0:.2f}".format(raise_amount)

print("Novo salario:", after_sal_out)
print("Reajuste ganho:", raise_amount_out)
print("Em percentual:", raise_percent, "%")
