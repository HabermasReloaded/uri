#include <iostream>


int main()
{
        float before_sal, after_sal, raise_ammount, raise_percent;
        std::cin >> before_sal;

        if (before_sal <= 400)
        {
                raise_percent = 15;
        }
        else if (before_sal < 800)
        {
                raise_percent = 12;
        }
        else if (before_sal < 1200)
        {
                raise_percent = 10;
        }
        else if (before_sal < 2000)
        {
                raise_percent = 7;
        }
        else
        {
                raise_percent = 5;
        }

        std::cout << before_sal << std::endl;

        return 0;
}
