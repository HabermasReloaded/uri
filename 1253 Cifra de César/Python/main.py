import string

letter_list = list(string.ascii_uppercase)  # list all uppercase alphabet

letter_val_list = range(23)

case_number = int(input())

for i in range(case_number):
    str_in = input()
    cipher_delta = int(input())

    str_out_indexes = []

    for letter in str_in:
        str_out_indexes.append(letter_list.index(letter) - cipher_delta)

    str_out = []

    for index in str_out_indexes:
        str_out.append(letter_list[index])

    print(str(str_out))
